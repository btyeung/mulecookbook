package com.confluex.examples.imperialmanufacturing;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "order")
public class DroidOrder {
	private String orderNumber;

	@XmlElement(name = "droid")
	private Collection<Item> items = new ArrayList<Item>();
	
	@XmlAttribute(name = "number")
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	
	public Collection<Item> getItems() {
		return new ArrayList<Item>(items);
	}
	
	public void addItem(final String type, final int quantity) {
		items.add(new Item() {{
			setType(type);
			setQuantity(quantity);
		}});
	}
	
	public static class Item {
		private String type;
		private int quantity;
        private String destination;

		@XmlAttribute
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		
		@XmlAttribute
		public int getQuantity() {
			return quantity;
		}
		public void setQuantity(int quantity) {
			this.quantity = quantity;
		}

        @XmlAttribute
        public String getDestination() {
            return destination;
        }
        public void setDestination(String destination) {
            this.destination = destination;
        }
		
	}
}
