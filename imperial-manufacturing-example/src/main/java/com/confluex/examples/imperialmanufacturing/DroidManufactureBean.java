package com.confluex.examples.imperialmanufacturing;

import org.springframework.stereotype.Service;

import com.confluex.examples.imperialmanufacturing.sdk.DroidFactorySdk;

@Service("droidManufacture")
public class DroidManufactureBean {
	
	private DroidFactorySdk droidFactorySdk;
	
	public void setDroidFactorySdk(DroidFactorySdk droidFactorySdk) {
		this.droidFactorySdk = droidFactorySdk;
	}

	public DroidOrder placeOrder(DroidOrder order) {
		order.setOrderNumber(droidFactorySdk.createOrder());

		// add the items
		
		droidFactorySdk.placeOrder(order.getOrderNumber());
		
		return order;
	}
}
