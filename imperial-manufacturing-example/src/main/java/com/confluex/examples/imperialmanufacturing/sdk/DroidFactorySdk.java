package com.confluex.examples.imperialmanufacturing.sdk;

public interface DroidFactorySdk {
	String createOrder();
	void addItem(String orderNumber, String type, int quantity);
	void placeOrder(String orderNumber);
}
