package com.confluex.examples.imperialmanufacturing.rest;

import java.net.URI;

import javax.annotation.Resource;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.confluex.examples.imperialmanufacturing.DroidManufactureBean;
import com.confluex.examples.imperialmanufacturing.DroidOrder;

@Path("/order")
@Service("orderResource")
public class OrderResource {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Context
	private UriInfo uriInfo;
	
	@Resource
	private DroidManufactureBean droidManufacture;
	
	@POST
	@Produces(MediaType.APPLICATION_XML)
	@Consumes(MediaType.APPLICATION_XML)
	public Response postOrder(DroidOrder order, @HeaderParam("xmlValid") String xmlValidHeader) {
		// If message is not flagged valid, return 400 BAD REQUEST
		if (! StringUtils.equals("valid", xmlValidHeader) ) return Response.status(Status.BAD_REQUEST).entity(order).build();
		
		order = droidManufacture.placeOrder(order);
		URI orderUri = uriInfo.getRequestUriBuilder().path(order.getOrderNumber()).build();
		return Response.created(orderUri).entity(order).build();
	}
}
